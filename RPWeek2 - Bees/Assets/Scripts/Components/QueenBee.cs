﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueenBee : MonoBehaviour
{
    public List<Bee> Bees = new List<Bee>();


    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.queenBee = this;

        InputHandler input = GetComponent<InputHandler>();
        if(input != null)
        {
            input.OnFireButtonPressed += CheckForInteractable;
        }
           
        SetupIntialBees();
    }

    void SetupIntialBees()
    {
        Bees.AddRange(GetComponentsInChildren<Bee>());

        foreach(Bee b in Bees)
        {
            b.SwapState(Bee.BeeState.swarm);
        }
    }

    void CheckForInteractable()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hit.collider != null)
        {
            Iinteractable interact = hit.collider.gameObject.GetComponent<Iinteractable>();
            if(interact != null)
            {
                if(Bees.Count != 0)
                {
                    interact.interact();
                    Bee b = Bees[0];
                    Bees.Remove(b);
                    b.FlyHome();
                }
            }      
        }   
    }

    private void OnDisable()
    {
        GetComponent<InputHandler>().OnFireButtonPressed -= CheckForInteractable;
    }
}
