﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisionHandler : MonoBehaviour,Iinteractable
{
    public void interact()
    {
        GetComponent<Health>().ChangeHealth(-1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Hive hive = collision.gameObject.GetComponent<Hive>();
        if(hive != null)
        {
            hive.OnAttacked();
        }
    }
}
