﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBar : MonoBehaviour
{
    LoadingBar bar;

    // Start is called before the first frame update
    void Start()
    {
        bar = GetComponent<LoadingBar>();
        Debug.Log(bar);
        ScoreHandler.OnScoreUpdated += UpdateBar;
       
    }

    void UpdateBar()
    {
        bar.UpdateBar(GameManager.instance.CurScore, GameManager.instance.stats.WinScore);
    }
}
