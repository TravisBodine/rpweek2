﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hive : MonoBehaviour
{

    public GameObject BeePrefab;

    LoadingBar beeProgressBar;

    float timer = 0;
    bool isLoading = false;
    // Start is called before the first frame update
    void Start()
    {
        beeProgressBar = GetComponentInChildren<LoadingBar>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLoading)
        {
            if (timer >= 0)
            {
                timer -= Time.deltaTime;
                beeProgressBar.UpdateBar(timer, GameManager.instance.stats.BeeSpawnSpeed);
            }
        }
    }

    public void OnAttacked()
    {
        GameManager.instance.HandleLoss();
    }

    private void SpawnBee()
    {
        GameObject bee = Instantiate(BeePrefab, transform.position, Quaternion.identity);
        bee.transform.parent = GameManager.instance.queenBee.gameObject.transform;

        Bee b = bee.GetComponent<Bee>();
        GameManager.instance.queenBee.Bees.Add(b);
        b.SwapState(Bee.BeeState.swarm);   
    }

    private void OnMouseEnter()
    {
        isLoading = true;
    }

    void OnMouseOver()
    {
        if(timer < GameManager.instance.stats.BeeSpawnSpeed)
            timer += Time.deltaTime;

        if (timer >= GameManager.instance.stats.BeeSpawnSpeed)
        {
            if(GameManager.instance.queenBee.Bees.Count < GameManager.instance.stats.MaxBees)
            {
                SpawnBee();
                timer = 0;
            }
            
        }
        beeProgressBar.UpdateBar(timer, GameManager.instance.stats.BeeSpawnSpeed);

    }
    private void OnMouseExit()
    {
        isLoading = false;
    }
}
