﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public float spawnRadius;

   public List<GameObject> Enemies;
   public List<GameObject> curEnemies;
 
   public List<GameObject> ScoreObjects;
   public List<GameObject> curScoreObjects;

    float timerS = 0;
    float timerE = 0;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SpawnEnemy();
        SpawnScoreObject();
    }

    private void SpawnScoreObject()
    {
        if(timerS < Time.time)
        {
            int index = Random.Range(0, ScoreObjects.Count - 1);

            GameObject obj = Instantiate(ScoreObjects[index], Random.insideUnitCircle * spawnRadius, Quaternion.identity);
            curScoreObjects.Add(obj);

            //need to change this later its spagetti
            obj.GetComponent<Flower>().spawner = this;
            timerS = Time.time + GameManager.instance.stats.ScoreObjectSpawnSpeed;
        }

    }

    private void SpawnEnemy()
    {
        if (timerE < Time.time) { 
            if (curEnemies.Count <= GameManager.instance.stats.MaxEnemies)
            {
                int index = Random.Range(0, Enemies.Count - 1);
                GameObject obj = Instantiate(Enemies[index], Random.insideUnitCircle.normalized * spawnRadius, Quaternion.identity);
                curEnemies.Add(obj);

                //need to change this later its spagetti
                obj.GetComponent<pawn_Enemy>().spawner = this;
            }
            timerE = Time.time + GameManager.instance.stats.EnemySpawnDelay;
        }
    }
}
