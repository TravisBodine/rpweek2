﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controller_Enemy : MonoBehaviour
{
    pawn_Enemy pawn;

    // Start is called before the first frame update
    void Start()
    {
        pawn = GetComponent<pawn_Enemy>();
    }

    // Update is called once per frame
    void Update()
    {
        pawn.RotateToLookAt(Vector3.zero);
        pawn.MoveForward();
    }
}
